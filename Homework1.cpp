﻿#include <iostream>

/* Задача: 
1) Создать класс Animal с публичным методом Voice() который
   выводит в консоль строку с текстом.
2) Наследовать от Animal минимум три класса (к примеру Dog, 
   Cat и т.д.) и в них перегрузить метод Voice() таким образом, 
   чтобы для примера в классе Dog метод Voice() выводил в
   консоль "Woof!".
3) В функции main создать массив указателей типа Animal и
   заполнить этот массив объектами созданных классов.
4) Затем пройтись циклом по массиву, вызывая на каждом
   элементе массива метод Voice().
*/


 
class Animal
{
public:
    Animal()
    {}
    virtual void Voice()
    {
        std::cout << "What I should say?" << std::endl;
    }
private:
    
};

class Dog : public Animal
{
public:
    Dog()
    {}
    void Voice() override
    {
        std::cout << "Woof!" << std::endl;
    }

private:

};

class Cat : public Animal
{
public:
    Cat()
    {}
    void Voice() override
    {
        std::cout << "Meow!" << std::endl;
    }

private:

};

class Cow : public Animal
{
public:
    Cow()
    {}
    void Voice() override
    {
        std::cout << "Moo!" << std::endl;
    }

private:

};

int main()
{
    
    Dog d;
    Cat ca;
    Cow co;

    Animal* Animals[3] = {&d, &ca, &co};
    
    std::cout << "Animals said: " << std::endl;
    for (int i = 0; i < 3; i++)
    {
        Animals[i]->Voice();
    }
}

